const assert = require('assert');
const app = require('../../src/app');

describe('\'helloworld\' service', () => {
  it('registered the service', () => {
    const service = app.service('helloworld');

    assert.ok(service, 'Registered the service');
  });
});
