'use strict';

module.exports = (sequelize, DataTypes) => {
  var test = sequelize.define('test', {
    id: DataTypes
  });

  // classMethods
  test.associate = function(models) {
    // associations can be defined here
  }

  // instanceMethods
  // test.prototype.somemethod();

  return test;
};