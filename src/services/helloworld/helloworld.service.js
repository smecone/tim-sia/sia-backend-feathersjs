// Initializes the `helloworld` service on path `/helloworld`
const createService = require('feathers-memory');
const hooks = require('./helloworld.hooks');

module.exports = function () {
  const app = this;
  const paginate = app.get('paginate');

  const options = {
    name: 'helloworld',
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/helloworld', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('helloworld');

  service.hooks(hooks);
};
